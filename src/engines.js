export default {
  raw:{
    label:'Brak transkrypcji',
    trans:e=>e
  },
  odpal:{
    label:'Tetowe usuwanie pal',
    trans:e=>e.replaceAll('p','').replaceAll('a','').replaceAll('l','')
  },
};
/*

<Grid item={true} xs={4}>
  <Button sx={{
      width: '100%'
    }} variant="contained" color="secondary" onClick={() => onSelect('br')}>Wybierz</Button>
</Grid>
<Grid item={true} xs={8}>
  Alfabet Białoruski
</Grid>
<Grid item={true} xs={4}>
  <Button sx={{
      width: '100%'
    }} variant="contained" color="secondary" onClick={() => onSelect('br')}>Wybierz</Button>
</Grid>
<Grid item={true} xs={8}>
  Alfabet Łemkowski
</Grid>
<Grid item={true} xs={4}>
  <Button sx={{
      width: '100%'
    }} variant="contained" color="secondary" onClick={() => onSelect('br')}>Wybierz</Button>
</Grid>
<Grid item={true} xs={8}>
  Alfabet Ormiański
</Grid>
<Grid item={true} xs={4}>
  <Button sx={{
      width: '100%'
    }} variant="contained" color="secondary" onClick={() => onSelect('br')}>Wybierz</Button>
</Grid>
<Grid item={true} xs={8}>
  Alfabet Rosyjski
</Grid>
<Grid item={true} xs={4}>
  <Button sx={{
      width: '100%'
    }} variant="contained" color="secondary" onClick={() => onSelect('br')}>Wybierz</Button>
</Grid>
<Grid item={true} xs={8}>
  Alfabet Ukraiński
</Grid>
<Grid item={true} xs={4}>
  <Button sx={{
      width: '100%'
    }} variant="contained" color="secondary" onClick={() => onSelect('br')}>Wybierz</Button>
</Grid>
<Grid item={true} xs={8}>
  Alfabet Hebrajski
</Grid>
<Grid item={true} xs={4}>
  <Button sx={{
      width: '100%'
    }} variant="contained" color="secondary" onClick={() => onSelect('br')}>Wybierz</Button>
</Grid>
<Grid item={true} xs={8}>
  Alfabet Idisz
</Grid>
*/
