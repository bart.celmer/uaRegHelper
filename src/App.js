import React, {Fragment, useState, useMemo} from 'react'

import {styled, createTheme, ThemeProvider} from '@mui/material/styles';

import AppBar from '@mui/material/AppBar';
import Badge from '@mui/material/Badge';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import Collapse from '@mui/material/Collapse';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import Divider from '@mui/material/Divider';
import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/IconButton';
import Link from '@mui/material/Link';
import List from '@mui/material/List';
import MenuIcon from '@mui/icons-material/Menu';
import MuiDrawer from '@mui/material/Drawer';
import NotificationsIcon from '@mui/icons-material/Notifications';
import Paper from '@mui/material/Paper';
import Step from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel';
import Stepper from '@mui/material/Stepper';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { plPL, ukUA,  } from '@mui/material/locale';

import Info from './Info';
import Form from './Form';
import TranscriptionSelect from './TranscriptionSelect';

import engines from './engines';

export default prosp => {
  const [data, setData] = useState({});
  const [fixedData, setFixedData] = useState({});
  const [locale, setLocale] = useState(plPL);
  const [step, setStep] = useState(0);
  const [trans, setTrans] = useState('raw');

  const theme = useMemo(()=>createTheme({
      palette: {
        primary: { main: 'rgb(8, 112, 209)' },
        secondary: { main: 'rgb(251, 237, 51)' },
      },
    },
    locale
  ),[locale]);

  const handleTranscriptionSelect = (trans) => {
    if(engines[trans]){
      setStep(2);
      setTrans(trans);
    }
  };

  const handleEndNow = ()=>{
    setData({});
    setStep(0);
  }

  const handleChange = (field,options={})=>(event)=>{
    const {raw=false, direct=false} = options;
    const value = direct?event:event.target.value;
    setData({...data,[field]:value});
    if(raw){
      setFixedData({...data,[field]:value});
    } else {
      setFixedData({...data,[field]:engines[trans].trans(value)});
    }
  }

  return (<ThemeProvider theme={theme}>
    <Box sx={{
        display: 'flex',
        mt: '92px'
      }}>
      <CssBaseline />
      <AppBar position="absolute">
        <Toolbar sx={{
            pr: '24px', // keep right padding when drawer closed
          }}>
          <Typography component="h1" variant="h6" color="inherit" noWrap="noWrap" sx={{
              flexGrow: 1
            }}>
            Generator formularzy rejestracyjnych
          </Typography>
          <Button variant="contained" color="secondary" onClick={handleEndNow}>Zakończ teraz</Button>
        </Toolbar>
      </AppBar>
      <Box sx={{p:2, width:'100%'}}>
        <Grid container spacing={1}>
          <Grid xs={12}>
            <Stepper activeStep={step}>
              <Step>
                <StepLabel>Zgody i informacje</StepLabel>
              </Step>
              <Step>
                <StepLabel>Wybór transkrypcji</StepLabel>
              </Step>
              <Step>
                <StepLabel>Wypełnij formularz</StepLabel>
              </Step>
              <Step>
                <StepLabel>Wydrukuj</StepLabel>
              </Step>
            </Stepper>
          </Grid>
            <Grid xs={12}>
              <Box sx={{p:1, width:'100%'}}>
                <Collapse in={step==0}>
                  <Info onAccept={()=>setStep(1)} />
                </Collapse>
                <Collapse in={step==1}>
                  <TranscriptionSelect onSelect={handleTranscriptionSelect} />
                </Collapse>
                <Collapse in={step==2}>
                  <Form data={data} fixedData={fixedData} handleChange={handleChange} />
                </Collapse>
              </Box>
          </Grid>
          <Grid xs={12}>
            <Box sx={{m:1,p:1}}><Divider /></Box>
          </Grid>
          <Grid xs={6} sx={{textAlign:'left'}}>
            <Button variant="contained" color="secondary" onClick={()=>setStep(i=>i-1)} disabled={(1>step)}>Wstecz</Button>
          </Grid>
          <Grid xs={6} sx={{textAlign:'right'}}>
          </Grid>
        </Grid>
      </Box>
    </Box>
  </ThemeProvider>);
}
