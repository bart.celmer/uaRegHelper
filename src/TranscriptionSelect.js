import React,{Fragment} from 'react'

import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';

import engines from './engines';

export default props => {
  const {onSelect} = props;
  return (<Box sx={{
      m: 1,
      p: 1
    }}>
    <Paper>
      <Box sx={{
          m: 1,
          p: 1
        }}>
        <Typography>
          <Grid container={true} spacing={3} justifyContent="center" alignItems="center">
            <Grid item={true} xs={12}>
              Przoszę określić alfabet w jakim zapisane są dane personalne - imie, nazwisko
            </Grid>
            {Object.entries(engines).map(([engine,{label}],no)=>(<Fragment key={engine}>
              <Grid item={true} xs={4}>
                <Button sx={{
                    width: '100%'
                  }} variant="contained" color="secondary" onClick={() => onSelect(engine)}>Wybierz</Button>
              </Grid>
              <Grid item={true} xs={8}>
                {label}
              </Grid>
            </Fragment>))}
          </Grid>
        </Typography>
      </Box>
    </Paper>
  </Box>);
}
