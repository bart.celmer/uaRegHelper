import React, {createContext, useContext} from 'react'

import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import Divider from '@mui/material/Divider';

import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

import TextField from '@mui/material/TextField';
import MobileDatePicker from '@mui/lab/MobileDatePicker';

import DateAdapter from '@mui/lab/AdapterMoment';

import LocalizationProvider from '@mui/lab/LocalizationProvider';

import engines from './engines';

const DataContext = createContext();


const GridTextInput = props => {
  const {
    label,
    field,
    xs = 12,
    md = 6,
    raw = false
  } = props;
  const {data, fixedData, handleChange} = useContext(DataContext);
  return (<Grid item={true} xs={xs} md={md}>
      <TextField sx={{
        width: '100%'
      }} label={label} value={data[field]} onChange={handleChange(field,{raw})} helperText={fixedData[field]}/>
  </Grid>);
}
const GridSelectInput = props => {
  const {
    label,
    field,
    xs = 12,
    md = 6,
    choices = []
  } = props;
  const {data, fixedData, handleChange} = useContext(DataContext);
  return (<Grid item={true} xs={xs} md={md}>
    <FormControl fullWidth="fullWidth">
      <InputLabel>{label}</InputLabel>
      <Select value={data[field]} onChange={handleChange(field, {raw: true})} label={label}>
        {
          Object.entries(choices).map(([
            value, label
          ], no) => (<MenuItem value={value}>{label}</MenuItem>))
        }
      </Select>
    </FormControl>
  </Grid>);
};
const GridDateInput = props => {
  const {
    label,
    field,
    xs = 12,
    md = 6
  } = props;
  const {data, fixedData, handleChange} = useContext(DataContext);
  return (<Grid item={true} xs={xs} md={md}>
    <MobileDatePicker inputFormat="MM/DD/yyyy" value={data[field]} onChange={handleChange(field, {
        raw: true,
        direct: true
      })} label={label} renderInput={(params) => <TextField {...params} sx={{
          width: '100%'
        }}/>}/>
  </Grid>);
};

const GridBreak = props => {
  return (<Grid item={true} xs={12}>
    <h3>{props.children}</h3>
    <Divider/> {
      props.subtitle && (<sup>
        <i>{props.subtitle}</i>
      </sup>)
    }
  </Grid>);
}
export default props => {
  const {data, fixedData, handleChange} = props;
  return (<Box sx={{
      m: 1,
      p: 1
    }}>
    <DataContext.Provider value={{
        data,
        fixedData,
        handleChange
      }}>
      <Paper>
        <LocalizationProvider dateAdapter={DateAdapter}>
          <Box sx={{
              m: 1,
              p: 1
            }}>
            <Typography>
              <Grid container={true} spacing={2}>
                <GridBreak>
                  1. Dane osoby, której ma zostać nadany numer PESEL / Дані про особу, якій присвоюється номер PESEL
                </GridBreak>
                <GridTextInput label="Imię (imiona) /Ім’я (імена)" field="imie"/>
                <GridTextInput label="Nazwisko / Прізвище" field="nazwisko"/>
                <GridDateInput label="Data urodzenia / Дата народження" field="data-urodzenia" item={true} xs={12} md={3}/>
                <GridTextInput md={3} label="Miejsce urodzenia /Місце народження" field="miejsce-urodzenia"/>
                <GridTextInput md={3} label="Kraj urodzenia /Країна народження" field="kraj-urodzenia"/>
                <GridTextInput md={3} label="Obywatelstwo / Громадянство" field="obywatelstwo"/>
                <GridSelectInput md={3} label="Płeć / Стать" field="plec" choices={{
                    'm' : 'mężczyzna / чоловік',
                    'k' : 'kobieta / жінка'
                  }}/>
                <GridDateInput field="data-wjazdu" item={true} xs={12} md={3} label="Data wjazdu na terytorium Polski / Дата в’їзду на територію Польщі - data"/>
                <GridTextInput label="Ukraiński numer ewidencyjny (o ile osoba posiada) / Український реєстраційний номер (Запис No)" field="ua-id"/>
                <GridTextInput md={12} raw={true} label="Oznaczenie dokumentu, na podstawie którego ustalono tożsamość / Позначення документа, на підставі якого встановлено особу" field="oznaczenie-dokumentu"/>
                <GridBreak subtitle="Wypełnij, jeżeli wniosek dotyczy dziecka i dane rodziców wynikają z dokumentów. / Заповнюйте, якщо заява стосується дитини, і дані батьків вказані в документах.">
                  Dane rodziców osoby małoletniej / Дані про батьків малолітньої особи
                </GridBreak>
                <GridTextInput md={4} field="imie-ojca" label="Imię ojca (pierwsze) / Ім’я батька (перше)"/>
                <GridTextInput md={4} field="nazwisko-ojca" label="Nazwisko ojca /Прізвище батька"/>
                <GridTextInput md={4} field="pesel-ojca" label="Numer PESEL (o ile został nadany) / Номер PESEL (якщо був присвоєний)"/>
                <GridTextInput md={4} field="imie-matki" label="Imię matki (pierwsze) / Ім’я матері (перше)"/>
                <GridTextInput md={4} field="nazwisko-matki" label="Nazwisko matki / Прізвище матерi"/>
                <GridTextInput md={4} field="pesel-matki" label="Numer PESEL (o ile został nadany) / Номер PESEL (якщо був присвоєний)"/>
                <GridBreak>
                  2. Dane osoby sprawującej faktyczną opiekę nad dzieckiem / Дані про особу, яка фактично здійснює опіку над дитиною
                </GridBreak>
                <GridTextInput label="Imię (imiona) /Ім’я (імена)" field="opiekun-imie"/>
                <GridTextInput label="Nazwisko / Прізвище" field="opiekun-nazwisko"/>
                <GridDateInput label="Data urodzenia / Дата народження" field="opiekun-data-urodzenia" item={true} xs={12} md={3}/>
                <GridTextInput md={3} label="Numer PESEL (o ile został nadany) / Номер PESEL (якщо був присвоєний)" field="opiekun-pesel"/>
                <GridSelectInput md={3} label="Płeć / Стать" field="opiekun-plec" choices={{
                    'm' : 'mężczyzna / чоловік',
                    'k' : 'kobieta / жінка'
                  }}/>
                <GridTextInput md={3} label="Obywatelstwo / Громадянство" field="opiekun-obywatelstwo"/>
                <GridTextInput md={6} raw={true} label="Oznaczenie dokumentu, na podstawie którego ustalono tożsamość / Позначення документа, на підставі якого встановлено особу" field="opiekun-ua-id"/>
                <GridSelectInput md={6} label="Relacje osoby sprawującej faktyczną opiekę nad dzieckiem / Відношення особи, яка фактично здійснює опіку над дитиною" field="opiekun-relacja" choices={{
                    'matka' : 'matka / матір',
                    'ojciec' : 'ojciec / батько',
                    'opiekun tymczasowy' : 'opiekun tymczasowy na podstawie orzeczenia polskiego sądu / тимчасовий опікун на підставі рішення польського суду',
                    'opiekun stały' : 'osoba sprawująca zastępczą opiekę nad dzieckiem na podstawie orzeczenia polskiego sądu / особа, яка здійснює патронажну опіку на підставі рішення польського суду',
                    'inna' : 'inna osoba / інша особа'
                  }}/>
                {data["opiekun-relacja"]=='inna'&&(<GridTextInput md={12} label="inna osoba sprawująca faktyczną opiekę nad dzieckiem np. siostra, babcia / інша особа, яка фактично здійснює опіку над дитиною, напр., сестра, бабуся" field="opiekun-relacja-inna"/>)}
              </Grid>
            </Typography>
          </Box>
        </LocalizationProvider>
      </Paper>
    </DataContext.Provider>
  </Box>);
}
