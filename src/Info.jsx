import React from 'react'

import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';

export default props => {
  const {onAccept} = props;
  return (<Box sx={{m:1,p:1}}>
    <Paper>
      <Box sx={{m:1,p:1}}>
        <Typography>
          <Grid container={true} spacing={1}>
            <Grid item={true} xs={12} sm={6}>
              <p>
                Ta aplikacja pomoże Ci wprowadzić dane i wydrukować formularz zgłoszenia.
              </p>
              <p>
                Ta aplikacja może gromadzić dane kontaktowe, przed zapisaniem ich zostanie wyświetlona informacj.
              </p>
              <p>
                Autor dołożył strań, aby aplikacja była pomocna i wolna od błędów, jeśli jednak masz jakieś uwagi daj proszę znać na bart.celmer@gmail.com.
              </p>
            </Grid>
            <Grid item={true} xs={12} sm={6}>
              <p>
                Ця програма допоможе вам ввести дані та роздрукувати форму заявки.
               </p>
               <p>
                Ця програма може збирати контактні дані, ви будете проінформовані перед їх збереженням.
               </p>
               <p>
                Автор зробив внесок у програму, щоб вона була корисною та безпомилковою, але якщо у вас є коментарі, будь ласка, повідомте мене на bart.celmer@gmail.com.
               </p>
            </Grid>
          </Grid>
        </Typography>
        <Button sx={{width:'100%'}} variant="contained" color="secondary" onClick={()=>onAccept()}>Rozpocznij / почати</Button>
      </Box>
    </Paper>
  </Box>);
}
